#!/bin/bash

#assign the command line arguments
JOBID=$1 # ProcID, number after queue command

################################################################################
# FIND SCRIPT LOCATION
################################################################################

SCRIPT_LOCATION="$0"

while [ -L "$SCRIPT_LOCATION" ]; do
  SCRIPT_LOCATION=$(readlink -e "$SCRIPT_LOCATION")
done

cd "$(dirname "$SCRIPT_LOCATION")" || exit 2

SCRIPT_DIR=$(pwd)

cd "${OLDPWD}" || exit 2

################################################################################
# CHANGE ME ACCORDING TO YOUR NEEDS
################################################################################

CMSSWREL='CMSSW_14_1_0_pre3'
OUTDIR='/afs/cern.ch/user/o/omiguelc/eos/EMTFPP'

################################################################################
#PRINT THE ARGUMENTS SUMMARY
################################################################################

echo '###########################################################################'
echo 'Job Id: '${JOBID}
echo 'CMSSW Release: ' ${CMSSWREL}
echo 'Output Directory: ' ${OUTDIR}
echo '###########################################################################'

# Create Output Directory
mkdir -p "$OUTDIR"

################################################################################
#SETUP CMSSW FRAMEWORK
################################################################################

INITDIR=`pwd`
BASEDIR="$INITDIR/tmp"
CMSSWDIR="$BASEDIR/$CMSSWREL/src"

# Create Base Directory
mkdir -p "$BASEDIR"
cd "$BASEDIR"

# Get CMSSW Release
cmsrel ${CMSSWREL}

# Enter CMSSW Release
cd "$CMSSWDIR"

# Extract sandbox
tar -xf "$INITDIR/sandbox.tar.gz"

# Run Scram
scram b -j32

################################################################################
##RUN THE ACTUAL SIMULATION
################################################################################

# RUN
echo "Running from: ${PWD}"

cmsenv
cmsRun "${INITDIR}/pset.py" \
    jobId=${JOBID} \
    inputFilesTxt="$INITDIR/input_files.txt" \
    outputDirectory=${OUTDIR}

