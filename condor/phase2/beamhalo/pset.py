import os
from FWCore.ParameterSet.VarParsing import VarParsing

# Here we have defined our own two VarParsing options
# add a list of strings for events to process
# this should be smaller than the total number of events in the file or otherwise get segfault when there are no more events in file
options = VarParsing('python')

options.register('jobId',
                 '0',
                 VarParsing.multiplicity.singleton,
                 VarParsing.varType.string,
                 "The job Id: 0")

options.register('inputFilesTxt',
                 'input_files.txt',
                 VarParsing.multiplicity.singleton,
                 VarParsing.varType.string,
                 "The input files")

options.register('outputDirectory',
                 '/afs/cern.ch/work/o/omiguelc/public/emtfpp',
                 VarParsing.multiplicity.singleton,
                 VarParsing.varType.string,
                 "The output directory")

options.parseArguments()

outputFile = options.outputDirectory + '/BeamHalo.' + str(options.jobId) + '.root'
print('Output File: %s' % outputFile)

with open(options.inputFilesTxt) as f:
    lines = [line.rstrip() for line in f]

inputFile = lines[int(options.jobId)]
print('Input File: %s' % inputFile)

# Auto generated configuration file
# using:
# Revision: 1.19
# Source: /local/reps/CMSSW/CMSSW/Configuration/Applications/python/ConfigBuilder.py,v
# with command line options: step2 -n 1000 -s SIM,DIGI,L1TrackTrigger,L1 --nThreads 8 --conditions auto:phase2_realistic --era Phase2C17I13M9 --geometry Extended2026D88 --filein file:data/hllhc_ir1_b2_beamhalo_nprim7380000_30.root --fileout file:out.root --eventcontent FEVTDEBUGHLT --pileup NoPileUp --beamspot HLLHC14TeV --datatier GEN-SIM-DIGI-RAW --customise SimGeneral/MixingModule/customiseStoredTPConfig.higherPtTP,SLHCUpgradeSimulations/Configuration/aging.customise_aging_1000,L1Trigger/Configuration/customisePhase2TTOn110.customisePhase2TTOn110,L1Trigger/L1TMuonEndCapPhase2/config.customise_mc,EMTFTools/NtupleMaker/config.customise_ntuple --customise_commands=process.TFileService = cms.Service('TFileService', fileName = cms.string('out.root'))\nprocess.gemRecHits.gemDigiLabel = 'simMuonGEMDigis'\nprocess.emtfToolsNtupleMaker.EMTFP2SimInfoEnabled = cms.bool(False)\nprocess.emtfToolsNtupleMaker.L1TrackTriggerTracksEnabled = cms.bool(False)\nprocess.emtfToolsNtupleMaker.TrackingParticlesEnabled = cms.bool(True)\nprocess.emtfToolsNtupleMaker.GenParticlesEnabled = cms.bool(False) --python_filename test/phase2/pset_BeamHalo.py --no_exec --mc
import FWCore.ParameterSet.Config as cms

from Configuration.Eras.Era_Phase2C17I13M9_cff import Phase2C17I13M9

process = cms.Process('L1',Phase2C17I13M9)

# import of standard configurations
process.load('Configuration.StandardSequences.Services_cff')
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.load('Configuration.EventContent.EventContent_cff')
process.load('SimGeneral.MixingModule.mixNoPU_cfi')
process.load('Configuration.Geometry.GeometryExtended2026D88Reco_cff')
process.load('Configuration.Geometry.GeometryExtended2026D88_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.SimIdeal_cff')
process.load('Configuration.StandardSequences.Digi_cff')
process.load('Configuration.StandardSequences.L1TrackTrigger_cff')
process.load('Configuration.StandardSequences.SimL1Emulator_cff')
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(-1),
    output = cms.optional.untracked.allowed(cms.int32,cms.PSet)
)

# Input source
process.source = cms.Source("PoolSource",
    dropDescendantsOfDroppedBranches = cms.untracked.bool(False),
    fileNames = cms.untracked.vstring('file:' + inputFile),
    inputCommands = cms.untracked.vstring('keep *'),
    secondaryFileNames = cms.untracked.vstring()
)

process.options = cms.untracked.PSet(
    IgnoreCompletely = cms.untracked.vstring(),
    Rethrow = cms.untracked.vstring(),
    TryToContinue = cms.untracked.vstring(),
    accelerators = cms.untracked.vstring('*'),
    allowUnscheduled = cms.obsolete.untracked.bool,
    canDeleteEarly = cms.untracked.vstring(),
    deleteNonConsumedUnscheduledModules = cms.untracked.bool(True),
    dumpOptions = cms.untracked.bool(False),
    emptyRunLumiMode = cms.obsolete.untracked.string,
    eventSetup = cms.untracked.PSet(
        forceNumberOfConcurrentIOVs = cms.untracked.PSet(
            allowAnyLabel_=cms.required.untracked.uint32
        ),
        numberOfConcurrentIOVs = cms.untracked.uint32(0)
    ),
    fileMode = cms.untracked.string('FULLMERGE'),
    forceEventSetupCacheClearOnNewRun = cms.untracked.bool(False),
    holdsReferencesToDeleteEarly = cms.untracked.VPSet(),
    makeTriggerResults = cms.obsolete.untracked.bool,
    modulesToCallForTryToContinue = cms.untracked.vstring(),
    modulesToIgnoreForDeleteEarly = cms.untracked.vstring(),
    numberOfConcurrentLuminosityBlocks = cms.untracked.uint32(0),
    numberOfConcurrentRuns = cms.untracked.uint32(1),
    numberOfStreams = cms.untracked.uint32(0),
    numberOfThreads = cms.untracked.uint32(1),
    printDependencies = cms.untracked.bool(False),
    sizeOfStackForThreadsInKB = cms.optional.untracked.uint32,
    throwIfIllegalParameter = cms.untracked.bool(True),
    wantSummary = cms.untracked.bool(False)
)

# Production Info
process.configurationMetadata = cms.untracked.PSet(
    annotation = cms.untracked.string('step2 nevts:1000'),
    name = cms.untracked.string('Applications'),
    version = cms.untracked.string('$Revision: 1.19 $')
)

# Output definition

process.FEVTDEBUGHLToutput = cms.OutputModule("PoolOutputModule",
    dataset = cms.untracked.PSet(
        dataTier = cms.untracked.string('GEN-SIM-DIGI-RAW'),
        filterName = cms.untracked.string('')
    ),
    fileName = cms.untracked.string('file:out.root'),
    outputCommands = process.FEVTDEBUGHLTEventContent.outputCommands,
    splitLevel = cms.untracked.int32(0)
)

# Additional output definition
process.load('SimG4Core.Application.g4SimHits_cfi')

process.g4SimHits.Generator.ApplyEtaCuts = cms.bool(False)
process.g4SimHits.Generator.MinPCut = cms.double(0.0001) #100keV
process.g4SimHits.Generator.BeamBkgdEvent = cms.untracked.bool(True)
process.g4SimHits.StackingAction.SaveFirstLevelSecondary = cms.untracked.bool(True)
process.g4SimHits.StackingAction.SavePrimaryDecayProductsAndConversionsInCalo = cms.untracked.bool(True)
process.g4SimHits.StackingAction.SavePrimaryDecayProductsAndConversionsInMuon = cms.untracked.bool(True)

# Other statements
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase2_realistic', '')

# Path and EndPath definitions
process.simulation_step = cms.Path(process.psim)
process.digitisation_step = cms.Path(process.pdigi)
process.L1TrackTrigger_step = cms.Path(process.L1TrackTrigger)
process.L1simulation_step = cms.Path(process.SimL1Emulator)
process.endjob_step = cms.EndPath(process.endOfProcess)
process.FEVTDEBUGHLToutput_step = cms.EndPath(process.FEVTDEBUGHLToutput)

# Schedule definition
process.schedule = cms.Schedule(process.simulation_step,process.digitisation_step,process.L1TrackTrigger_step,process.L1simulation_step,process.endjob_step,process.FEVTDEBUGHLToutput_step)
from PhysicsTools.PatAlgos.tools.helpers import associatePatAlgosToolsTask
associatePatAlgosToolsTask(process)

#Setup FWK for multithreaded
process.options.numberOfThreads = 16
process.options.numberOfStreams = 0

# customisation of the process.

# Automatic addition of the customisation function from SimGeneral.MixingModule.customiseStoredTPConfig
from SimGeneral.MixingModule.customiseStoredTPConfig import higherPtTP

#call to customisation function higherPtTP imported from SimGeneral.MixingModule.customiseStoredTPConfig
process = higherPtTP(process)

# Automatic addition of the customisation function from SLHCUpgradeSimulations.Configuration.aging
from SLHCUpgradeSimulations.Configuration.aging import customise_aging_1000

#call to customisation function customise_aging_1000 imported from SLHCUpgradeSimulations.Configuration.aging
process = customise_aging_1000(process)

# Automatic addition of the customisation function from L1Trigger.Configuration.customisePhase2TTOn110
from L1Trigger.Configuration.customisePhase2TTOn110 import customisePhase2TTOn110

#call to customisation function customisePhase2TTOn110 imported from L1Trigger.Configuration.customisePhase2TTOn110
process = customisePhase2TTOn110(process)

# Automatic addition of the customisation function from L1Trigger.L1TMuonEndCapPhase2.config
from L1Trigger.L1TMuonEndCapPhase2.config import customise_mc

#call to customisation function customise_mc imported from L1Trigger.L1TMuonEndCapPhase2.config
process = customise_mc(process)

# Automatic addition of the customisation function from EMTFTools.NtupleMaker.config
from EMTFTools.NtupleMaker.config import customise_ntuple

#call to customisation function customise_ntuple imported from EMTFTools.NtupleMaker.config
process = customise_ntuple(process)

# End of customisation functions


# Customisation from command line

process.TFileService = cms.Service('TFileService', fileName = cms.string(outputFile))
process.gemRecHits.gemDigiLabel = 'simMuonGEMDigis'
process.emtfToolsNtupleMaker.EMTFP2SimInfoEnabled = cms.bool(False)
process.emtfToolsNtupleMaker.L1TrackTriggerTracksEnabled = cms.bool(False)
process.emtfToolsNtupleMaker.TrackingParticlesEnabled = cms.bool(True)
process.emtfToolsNtupleMaker.GenParticlesEnabled = cms.bool(True)
# Add early deletion of temporary data products to reduce peak memory need
from Configuration.StandardSequences.earlyDeleteSettings_cff import customiseEarlyDelete
process = customiseEarlyDelete(process)
# End adding early deletion
