
from CRABClient.UserUtilities import config
config = config()

config.section_('General')
config.General.requestName = 'XTo2LongLivedTo4Mu'
config.General.workArea = 'logs'
config.General.transferOutputs = True  ## Do output root files
config.General.transferLogs = True

config.section_('JobType')
config.JobType.psetName = 'test/phase2/pset_XTo2LongLivedTo4Mu.py'
config.JobType.outputFiles = ['out.root'] ## Must be the same as the output file in process.TFileService in config.JobType.psetName python file
config.JobType.pluginName = 'PrivateMC'
config.JobType.numCores = 8
config.JobType.maxMemoryMB = 8000

config.section_('Data')
config.Data.splitting = 'EventBased'
config.Data.unitsPerJob = 1000  ## Should take ~10 minutes for 100k events
config.Data.totalUnits = 1000000  ## Should take ~10 minutes for 100k events
config.Data.publication = False
config.Data.outputDatasetTag = 'XTo2LongLivedTo4Mu'
config.Data.outLFNDirBase = '/store/user/omiguelc/EMTFPP'

config.section_('User')

config.section_('Site')
config.Site.storageSite = 'T3_CH_CERNBOX'
