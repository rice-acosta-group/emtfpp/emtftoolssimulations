mkdir -p test/run3
mkdir -p crab/run3

genfiles() {
    # Read Args
    samplename="${1}"
    genfragment_cff="$2"

    # Define variables
    crabtemp=`cat templates/crab_run3.py`
    output_path="test/run3/pset_${samplename}"

    # Custom
    customise_str='Configuration/DataProcessing/RecoTLR.customisePostEra_Run3'

    # Generate Pset
    cmsDriver.py "$genfragment_cff" \
        -n 1000 --nThreads 8 \
        -s GEN,SIM,DIGI,L1,DIGI2RAW,HLT \
        --conditions auto:phase1_2024_realistic --pileup NoPileUp --era Run3 \
        --fileout "file:raw.root" --eventcontent FEVTDEBUGHLT --datatier GEN-SIM-DIGI-RAW \
        --python_filename "${output_path}_GEN_SIM_DIGI_RAW.py" \
        --no_exec \
        --mc

    cmsDriver.py customL1toNANO \
        -n 1000 --nThreads 8 \
        -s RAW2DIGI,L1Reco,RECO,PAT,NANO:@PHYS+@L1DPG  \
        --conditions auto:phase1_2024_realistic --pileup NoPileUp --era Run3 \
        --filein "file:raw.root" --fileout "file:out.root" \
        --eventcontent NANOAOD --datatier NANOAOD \
        --customise "$customise_str" \
        --python_filename "${output_path}_L1T_NANO.py" \
        --no_exec \
        --mc

    # # Generate Crab file
    # crabcont=`echo "$crabtemp" | sed -e 's/\$SAMPLE_NAME/'"${samplename}/g"`
    # echo "$crabcont" > "crab/phase2/$samplename.py"
}

# User Inputs
# samplename="SingleMuon_PosEnd_FlatInvpt_2GeV"
# genfragment_cff="EMTFTools/ParticleGuns/SingleMuon_PosEnd_FlatInvpt_2GeV_cfi.py"
# genfiles "$samplename" "$genfragment_cff"

# samplename="SingleMuon_NegEnd_FlatInvpt_2GeV"
# genfragment_cff="EMTFTools/ParticleGuns/SingleMuon_NegEnd_FlatInvpt_2GeV_cfi.py"
# genfiles "$samplename" "$genfragment_cff"

samplename="XTo2LongLivedTo4Mu"
genfragment_cff="EMTFTools/ParticleGuns/LLP_flatMass_cfi.py"
genfiles "$samplename" "$genfragment_cff"

# samplename="SingleMuon_PosEnd_FlatPt_2GeV"
# genfragment_cff="EMTFTools/ParticleGuns/SingleMuon_PosEnd_FlatPt_2GeV_cfi.py"
# genfiles "$samplename" "$genfragment_cff"

# samplename="SingleMuon_NegEnd_FlatPt_2GeV"
# genfragment_cff="EMTFTools/ParticleGuns/SingleMuon_NegEnd_FlatPt_2GeV_cfi.py"
# genfiles "$samplename" "$genfragment_cff"

